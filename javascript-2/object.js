var schoolObj = {
  nama: "Sekolah Menengah",
  tingkatan: "SMP",
  domisili: "Jakarta",
  jumlah_kelas: 15,
};

// Cara 1
// console.log(schoolObj.jumlah kelas);

// Cara 2
// console.log(schoolObj["jumlah-kelas"]);

// Cara 1 Assign Value
// schoolObj.nama = "Sekolah Dasar";
// schoolObj.akreditasi = "A";

// Cara 2 Assign Value
// schoolObj['nama'] = "Sekolah Dasar";

delete schoolObj.jumlah_kelas;
delete schoolObj["domisili"];

console.log(schoolObj);
